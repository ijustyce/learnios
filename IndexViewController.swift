//
//  ViewController.swift
//  learnIos
//
//  Created by 杨春 on 16/7/20.
//  Copyright © 2016年 com.ijustyce. All rights reserved.
//

import UIKit

class IndexViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func goBack(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func gotoListView(sender: AnyObject) {
        performSegueWithIdentifier("listView", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
