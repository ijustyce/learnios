//
//  StringUtils.swift
//  learnIos
//
//  Created by 杨春 on 16/7/22.
//  Copyright © 2016年 com.ijustyce. All rights reserved.
//

import UIKit
class StringUtils{
    public static func isEmpty(values : String...) -> BooleanType {
        for value in values {
            if value.characters.count == 0 {
                return true
            }
        }
        return false
    }
}
