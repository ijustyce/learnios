//
//  HttpParams.swift
//  learnIos
//
//  Created by 杨春 on 16/7/27.
//  Copyright © 2016年 com.ijustyce. All rights reserved.
//

import Foundation

public class HttpParams{
    
    private var tag : String
    private var url : String
    
    private var cacheKey : String = ""
    private var cacheTime : Int = -1
    
    private var params : [String : String] = [ : ]
    
    private init(tag : String, url : String){
        self.url = url
        self.tag = tag
    }
    
    public static func create(tag : String, url : String) -> HttpParams{
        return HttpParams(tag: tag, url: url);
    }
    
    public func add(key : String, value : String) -> HttpParams{
        params[key] = value
        return self
    }
    
    public func addCacheKey(value : String) -> HttpParams{
        cacheKey += value
        if cacheTime < 0 {
            cacheTime = 60
        }
        return self
    }
    
    public func setCacheTime(value : Int) -> HttpParams{
        if value < 0 {
            return self
        }
        cacheTime = value
        return self
    }
}
