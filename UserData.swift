//
//  UserData.swift
//  learnIos
//
//  Created by 杨春 on 16/7/22.
//  Copyright © 2016年 com.ijustyce. All rights reserved.
//

import UIKit
class UserData {
    
    static var userRegisterInfo : [String : String] = [:]
    
    public static func addUser(userId : String, pw : String){
        userRegisterInfo[userId] = pw
    }
    
    public static func checkLogin(userId: String, pw: String) -> BooleanType{
        for (key, value) in userRegisterInfo {
            if key == userId && value == pw {
                return true
            }
        }
        return false
    }
}
