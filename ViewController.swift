//
//  ViewController.swift
//  learnIos
//
//  Created by 杨春 on 16/7/20.
//  Copyright © 2016年 com.ijustyce. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var userId: UITextField!
    
    @IBAction func doLogin(sender: AnyObject) {
        
        INetWork.sendGet()
        
        let userId : String = self.userId.text!
        let password : String = self.password.text!
        if UserData.checkLogin(userId, pw: password){
            let sendStoryBoard : UIStoryboard = UIStoryboard.init(name: "Index", bundle: nil)
            let indexView : UIViewController = sendStoryBoard.instantiateViewControllerWithIdentifier("Index")
            presentViewController(indexView, animated: true, completion:nil )
            navigationController?.pushViewController(indexView, animated: true)
            return
        }
        DialogUtils.showDialog("userId:\(userId) password:\(password)", title: "登录", controller: self)
    }
    
    @IBOutlet weak var registerId: UITextField!
    @IBOutlet weak var registerPw: UITextField!
    @IBOutlet weak var registerPwAgain: UITextField!
    
    @IBAction func doRegister(sender: AnyObject) {
        let userId : String = registerId.text!
        let pw : String = registerPw.text!
        let pwAgain : String = registerPwAgain.text!
        if StringUtils.isEmpty(userId, pw, pwAgain){
            DialogUtils.showDialog("输入不能为空！", title: "注册", controller: self)
        }
        if pw != pwAgain {
            DialogUtils.showDialog("两次密码输入不一致", title: "注册", controller: self)
        }
        UserData.addUser(userId, pw: pw)
        DialogUtils.showDialog("注册成功，请登录", title: "注册", controller: self, action: {
            action in
            self.dismissViewControllerAnimated(true, completion: nil)
        })
    }
    
    @IBAction func register(sender: AnyObject) {
        performSegueWithIdentifier("register", sender: self)
    }
    
    @IBAction func backToLogin(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func dimissKeyBoard(sender: AnyObject) {
        password.resignFirstResponder();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
