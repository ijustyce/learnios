//
//  INetWork.swift
//  learnIos
//
//  Created by 杨春 on 16/7/27.
//  Copyright © 2016年 com.ijustyce. All rights reserved.
//

import Foundation
import Alamofire

public class INetWork{
    
    public static func sendGet(){
        Alamofire.request(.GET, "https://httpbin.org/get", parameters: ["foo": "bar"])
            .responseJSON { response in
            
            //   print(response.request)  // original URL request
            //    print(response.response) // URL response
            //    print(response.data)     // server data
            //    print(response.result)   // result of response serialization
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                }
        }
    }
}
