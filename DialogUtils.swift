//
//  DialogUtils.swift
//  learnIos
//
//  Created by 杨春 on 16/7/22.
//  Copyright © 2016年 com.ijustyce. All rights reserved.
//

import UIKit
class DialogUtils {
    
    public static func showDialog(msg:String, title:String, controller: UIViewController){
        showDialog(msg, title: title, controller: controller, action: { (UIAlertAction) -> Void in})
    }
    
    public static func showDialog(msg:String, title:String, controller: UIViewController, action: ((UIAlertAction)->Void)){
        let alertViewAction: UIAlertAction = UIAlertAction.init(title: "确定", style: UIAlertActionStyle.Default, handler: action)
        let alertViewCancelAction: UIAlertAction = UIAlertAction.init(title: "取消", style: UIAlertActionStyle.Cancel, handler: nil)
        let alertController = UIAlertController(title: title, message: msg, preferredStyle:UIAlertControllerStyle.Alert)
        alertController.addAction(alertViewAction)
        alertController.addAction(alertViewCancelAction)
        controller.presentViewController(alertController, animated: true, completion: nil)
    }
}
